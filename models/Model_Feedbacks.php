<?php

//require_once "../classes/Model_Base.php";
// модель
Class Model_Feedbacks Extends Model_Base {


    public $id;
    public $name;
    public $email;
    public $message;
    public $date;

	public function fieldsTable(){
		return array(
            'id'=>'id',
            'name'=>'name',
            'email'=>'email',
            'message'=>'message',
            'date'=>'date',
            'confirmation'=>'confirmation',
            'edited'=>'edited',
        );
	}

}