<?php

?>
<!DOCTYPE html>

<head>
    <title> Форма обратной связи </title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="language" content="ru"/>
    <meta name="Author" content="Sergey Sidorenko">
    <meta name="Keywords"
          content="feedback form">
    <meta name="description" content="feedback form">
    <!-- GOOGLE FONTS -->
    <link
        href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900'
        rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic' rel='stylesheet'
          type='text/css'>
    <!--    /////-->
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
    <!--    /////-->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <!-- Optional theme -->
    <link rel="stylesheet" href="../../css/bootstrap-theme.min.css">
    <link rel="stylesheet" href="../../css/style.css">
    <link rel="stylesheet" href="../../css/style2.css">
    <link rel="stylesheet" href="../../css/style_newsletter.css">
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <!-- menu css -->
    <link id="themeLink" rel="stylesheet" href="../../css/menu.css">
    <!-- TOPMENU -->
    <link id="main-css" href="../../css/menu_top.css" type="text/css" rel="stylesheet">
    <!-- CSS STYLE -->
    <link rel="stylesheet" type="text/css" href="../../css/style_exeples.css" media="screen"/>
    <!-- THE PREVIEW STYLE SHEETS, NO NEED TO LOAD IN YOUR DOM -->
    <link rel="stylesheet" type="text/css" href="css/navstylechange.css" media="screen"/>
    <!-- THE ICON FONTS -->
    <link rel="stylesheet" href="css/forms.css" type="text/css">
    <link rel="stylesheet" href="css/category.css" type="text/css">
    <link rel="stylesheet" href="css/content.css" type="text/css">
    <link rel="stylesheet" href="css/style-detail.css" type="text/css">
    <link rel="stylesheet" href="css/slickmap.css" type="text/css">
    <link rel="stylesheet" href="css/guides.css" type="text/css">
    <link rel="stylesheet" href="../../css/feedback.css" type="text/css">
    <!--     get jQuery from the google apis -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
    <script src="../../js/sortTable.v2.1.min.js"></script>
    <script src="../../js/sortTable.v2.1.src.js"></script>
</head>
<body>


<!--<h1>Форма обратной связи</h1>-->
<?php
	include ($contentPage);
?>
<footer>
</footer>
</body>
</html>