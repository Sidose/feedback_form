<!--Test view <br/>-->
<!--id: --><?//=$userInfo['id'];?><!--<br/>-->
<!--name: --><?//=$userInfo['name'];?>
<h1>Панель администратора</h1>
<?php
//var_dump(__DIR__);die;
?>
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <?php if ($feedbacks) {?>
        <table id="target" style=" width:400px; margin:0 auto;" class="table table-bordered table-hover">
            <thead>
            <tr>
                <th>Номер</th>
                <th>Имя</th>
                <th>E-mail</th>
                <th>Сообщение</th>
                <th>Дата</th>
                <th>Редактировать</th>
            </tr>
            </thead>
            <?php  for($i = 0, $k = count($feedbacks); $i < count($feedbacks); ++$i) { ?>
                <tr>
                    <td><?= $k--?></td>
                    <td><?= $feedbacks[$i]['name']?></td>
                    <td><?= $feedbacks[$i]['email']?></td>
                    <td><?= $feedbacks[$i]['message']?></td>
                    <td><?= $feedbacks[$i]['date']?></td>
                    <td>
                        <a href="edit.php?id=<?= $feedbacks[$i]['id'] ?>"><span
                                class="glyphicon glyphicon-pencil" title="редактировать"></span></a>
                    </td>
                </tr>
            <?php } ?>
        </table>
    <?php } else {?>
        <h2>Пока отзывов нет</h2>
    <?php } ?>


    <script>
        var sortAsc  = document.createElement("SPAN"),
            sortDesc = document.createElement("SPAN");

        sortAsc.innerHTML  = "&#9660;";
        sortDesc.innerHTML = "&#9650;";

        sortTable(document.getElementById("target"), sortAsc, sortDesc);
    </script>

