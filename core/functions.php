<?php

require_once "../config.php";
require_once "../classes/Model_Base.php";
require_once "../models/Model_Feedbacks.php";
//var_dump(777);die;

$dbObject = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME, DB_USER, DB_PASS);
$dbObject->exec('SET CHARACTER SET utf8');
$model = new Model_Feedbacks();
$data = secureData(array_merge($_POST,$_GET));
function secureData($data) {
    foreach($data as $key => $value){
        if(is_array($value)) $this->secureData($value);
        else $data[$key] = htmlspecialchars($value);
    }
    return $data;
}


    if($data["feedbackForm"]) {
        $model->saveToDB($data);
        $r = '../index.php';
    }
    if($data["enterAdminForm"]) {
        if($model->getAdmin($data)){
            $r = 'admin/index.php';
        } else {
            $r = $_SERVER['HTTP_REFERER'];
        }

}
if($data["editForm"]) {
    $model->editMessage($data);
    $r = 'edit.php';
}
    else {
    }

header("Location: $r");
exit;
