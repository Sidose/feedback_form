<?php
// контролер
Class Controller_Index Extends Controller_Base {
	
	// шаблон
	public $layouts = "first_layouts";
	
	// экшен
	function index() {
        $select = array(
            'where' => "confirmation = 1",
            'order' =>"date DESC",// условие
        );
		$model = new Model_Feedbacks($select);
		$feedbacks = $model->getAllRows();

        $this->template->vars('feedbacks', $feedbacks);
		$this->template->view('index');
	}
	
}